#!/bin/bash
RED='\033[0;31m'
GREEN='\033[0;32m'
CYAN='\033[0;36m'
NC='\033[0m' # No Color

domain=`cat /root/domain`
echo -e "${CYAN}Enter the subdomain for the site on $domain to DELETE: ${NC}"
read subdomain
fqdn="$subdomain.$domain"
folder="/var/www/$fqdn"
echo -e "${CYAN}Calling Apache2 to disable $fqdn${NC}"
a2dissite "$fqdn.conf"
unlink "/etc/apache2/sites-available/$fqdn.conf"
a2dissite "$fqdn-le-ssl.conf"
unlink "/etc/apache2/sites-available/$fqdn-le-ssl.conf"
echo -e "${GREEN}Done.${NC}"
echo -e "${CYAN}Restarting Apache2.${NC}"
service apache2 reload
echo -e "${GREEN}Done.${NC}"
echo -e "${CYAN}Deleting root folder $folder${NC}"
rm -rf "$folder"
echo -e "${GREEN}Done.${NC}"
