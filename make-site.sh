#!/bin/bash
RED='\033[0;31m'
GREEN='\033[0;32m'
CYAN='\033[0;36m'
NC='\033[0m' # No Color

domain=`cat /root/domain`
echo -e "${CYAN}Enter the subdomain for the new site on $domain: ${NC}"
read subdomain
fqdn="$subdomain.$domain"
folder="/var/www/$fqdn"
echo -e "${CYAN}Creating root folder $folder{NC}"
mkdir "$folder"
echo -e "${GREEN}Done.${NC}"

echo -e "${CYAN}Creating index file at $folder/index.html${NC}"
cat > "$folder/index.html" <<EOF
<!doctype html>
<html lang="en-us">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>$fqdn</title>
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <style>
            html, body {background-color: #fff; color: #636b6f; font-family: 'Raleway', sans-serif; font-weight: 100; height: 100vh; margin: 0;}
            .full-height {height: 100vh;}
            .flex-center {align-items: center; display: flex; justify-content: center;}
            .position-ref {position: relative;}
            .top-right {position: absolute; right: 10px; top: 18px;}
            .content {text-align: center;}
            .title {font-size: 84px;}
            .links > a {color: #636b6f;padding: 0 25px;font-size: 12px;font-weight: 600;letter-spacing: .1rem;text-decoration: none;text-transform: uppercase;}
            .m-b-md {margin-bottom: 30px;}
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            <div class="content">
                <div class="title m-b-md">$fqdn</div>
            </div>
        </div>
    </body>
</html>

EOF
chown www-data:www-data "$folder"
echo -e "${GREEN}Done.${NC}"

echo -e "${CYAN}Writing config files${NC}"
cat > "/etc/apache2/sites-available/$fqdn.conf" <<EOF
<VirtualHost *:80>
    DocumentRoot "$folder"
    ServerName $fqdn
    <Directory "$folder">
        AllowOverride All
        allow from all
        Options +Indexes
    </Directory>
    RewriteEngine on
    RewriteRule ^ https://%{SERVER_NAME}%{REQUEST_URI} [END,NE,R=permanent]
</VirtualHost>
EOF
cat > "/etc/apache2/sites-available/$fqdn-le-ssl.conf" <<EOF
<IfModule mod_ssl.c>
    <VirtualHost *:443>
        DocumentRoot "$folder"
        ServerName $fqdn
        <Directory "$folder">
            AllowOverride All
            allow from all
            Options +Indexes
        </Directory>
        SSLCertificateFile /etc/letsencrypt/live/$domain/fullchain.pem
        SSLCertificateKeyFile /etc/letsencrypt/live/$domain/privkey.pem
        Include /etc/letsencrypt/options-ssl-apache.conf
        Header always set Strict-Transport-Security "max-age=31536000"
    </VirtualHost>
</IfModule>
EOF
echo -e "${GREEN}Done.${NC}"

echo -e "${CYAN}Calling Apache2 to enable $fqdn${NC}"
a2ensite "$fqdn.conf"
a2ensite "$fqdn-le-ssl.conf"
service apache2 reload
echo -e "${GREEN}Done.${NC}"
